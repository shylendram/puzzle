package com.shylubalu.puzzle.domain;

import java.util.List;

/**
 * Created by Shail on 30-Apr-17.
 */

public class PuzzleResponse {

    private List<Puzzle> puzzleList = null;

    public List<Puzzle> getPuzzleList() {
        return puzzleList;
    }

    public void setPuzzleList(List<Puzzle> puzzleList) {
        this.puzzleList = puzzleList;
    }
}
