package com.shylubalu.puzzle.api;

import com.shylubalu.puzzle.domain.PuzzleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HEAD;

/**
 * Created by ssv-526 on 3/15/2017.
 */

public interface ApiInterface {

    @HEAD("puzzles/list/")
    Call<Void> getEventHead();

    @GET("puzzles/list/")
    Call<PuzzleResponse> getPuzzles();

}
