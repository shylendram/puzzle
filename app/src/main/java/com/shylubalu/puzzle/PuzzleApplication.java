package com.shylubalu.puzzle;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Shail on 30-Apr-17.
 */

public class PuzzleApplication extends Application {

    public static ProgressDialog progressDialog;
    public static AlertDialog alertDialog;
    public static Realm realm;
    public static RealmConfiguration config;
    private static Toast toast;

    @Override
    public void onCreate() {
        super.onCreate();

        realm.init(this);

//        RealmConfiguration config = new RealmConfiguration.Builder().build();
//        Realm.setDefaultConfiguration(config);

        config = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static Realm getRealmInstance() {
        // Use the config
        realm = Realm.getInstance(config);
        return realm;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static void showNoInternetDialog(final Activity context) {
        alertDialog = new AlertDialog.Builder(context)
                .setMessage("No internet available. Click OK to go settings, enable WiFi")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent callWiFiSettingIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        context.startActivityForResult(callWiFiSettingIntent, 0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();
        if (alertDialog != null && !alertDialog.isShowing()) {
            alertDialog.show();
        }

    }

    public static ProgressDialog showProgressDialog(Context context, String title) {
        // create a progress bar while the video file is loading
        progressDialog = new ProgressDialog(context);
        // set a title for the progress bar
        progressDialog.setTitle(title);
        // set a message for the progress bar
        progressDialog.setMessage("Please wait...");
        //set the progress bar not cancelable on users' touch
        progressDialog.setCancelable(true);
        // show the progress bar
        progressDialog.show();

        return progressDialog;
    }

    public static void hideProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void showToast(final String msg, int duration, Context context) {
        toast = Toast.makeText(context, msg, duration);
        toast.setText(msg);
        toast.setDuration(duration);
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public static void showAlertDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        realm.close();
    }

}
