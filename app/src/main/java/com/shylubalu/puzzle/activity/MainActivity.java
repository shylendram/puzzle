package com.shylubalu.puzzle.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.shylubalu.puzzle.PuzzleApplication;
import com.shylubalu.puzzle.R;
import com.shylubalu.puzzle.adapter.PuzzleAdapter;
import com.shylubalu.puzzle.api.ApiInterface;
import com.shylubalu.puzzle.domain.Puzzle;
import com.shylubalu.puzzle.domain.PuzzleResponse;
import com.shylubalu.puzzle.utils.ConnectivityReceiver;
import com.shylubalu.puzzle.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.shylubalu.puzzle.PuzzleApplication.hideProgressDialog;
import static com.shylubalu.puzzle.PuzzleApplication.showNoInternetDialog;
import static com.shylubalu.puzzle.PuzzleApplication.showProgressDialog;
import static com.shylubalu.puzzle.PuzzleApplication.showToast;
import static com.shylubalu.puzzle.utils.RestService.getInterfaceService;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ProgressDialog progressDialog;
    private Realm mRealm;
    private List<Puzzle> puzzleList = new ArrayList<>();
    private PuzzleAdapter puzzleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        getPuzzles();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void getPuzzles() {
        if (ConnectivityReceiver.isNetworkAvailable(getApplicationContext())) { // load from server
            loadFromServer();
        } else { // load from local
            loadFromLocal();
        }
    }

    private void loadFromLocal() {

        try {
            List<Puzzle> puzzlesFromLocal = getPuzzlesFromLocal();
            if (puzzlesFromLocal.size() > 0) {
                puzzleList = puzzlesFromLocal;
                setPuzzlesToAdapter(puzzleList);
            } else {
                showToast("No net and no puzzles to load", 0, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Puzzle> getPuzzlesFromLocal() {
        return mRealm.where(Puzzle.class).findAll();
    }

    private void loadFromServer() {

        final ApiInterface interfaceService = getInterfaceService();

        try {

            progressDialog = showProgressDialog(getApplicationContext(), "Getting puzzles from server");
            if (ConnectivityReceiver.isNetworkAvailable(this)) {
                Call<PuzzleResponse> puzzles = interfaceService.getPuzzles();

                puzzles.enqueue(new Callback<PuzzleResponse>() {
                    @Override
                    public void onResponse(Call<PuzzleResponse> call, Response<PuzzleResponse> response) {
                        if (response.code() == 200) {
                            PuzzleResponse puzzleResponse = response.body();
                            puzzleList = puzzleResponse.getPuzzleList();

                            if (puzzleList.size() > 0) {

                                saveInLocal(puzzleList);

                                setPuzzlesToAdapter(puzzleList);
                            }
                        } else {
                            hideProgressDialog(progressDialog);
                        }
                    }

                    @Override
                    public void onFailure(Call<PuzzleResponse> call, Throwable t) {
                        t.printStackTrace();
                        hideProgressDialog(progressDialog);
                    }
                });
            } else {
                showNoInternetDialog(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressDialog(progressDialog);
        }

    }

    private void setPuzzlesToAdapter(List<Puzzle> puzzleList) {
        puzzleAdapter = new PuzzleAdapter(puzzleList, getApplicationContext());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(puzzleAdapter);

        puzzleAdapter.notifyDataSetChanged();
    }

    private void saveInLocal(List<Puzzle> puzzleList) {

        mRealm = PuzzleApplication.getRealmInstance();

        for (Puzzle puzzle : puzzleList) {

            Puzzle puzzle1 = mRealm.where(Puzzle.class).equalTo("_id", puzzle.get_id()).findFirst();

            mRealm.beginTransaction();

            if (puzzle1 != null) {
                mRealm.insertOrUpdate(puzzle);
            } else {
                mRealm.copyToRealm(puzzle);
            }

            mRealm.commitTransaction();
        }
    }
}
