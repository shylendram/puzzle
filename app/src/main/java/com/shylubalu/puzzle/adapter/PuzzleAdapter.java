package com.shylubalu.puzzle.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shylubalu.puzzle.R;
import com.shylubalu.puzzle.domain.Puzzle;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shail on 30-Apr-17.
 */

public class PuzzleAdapter extends RecyclerView.Adapter<PuzzleAdapter.MyViewHolder> {

    List<Puzzle> puzzleList = new ArrayList<>();
    Context context;

    public PuzzleAdapter(List<Puzzle> puzzleList, Context context) {
        this.puzzleList.clear();
        this.puzzleList = puzzleList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PuzzleAdapter.MyViewHolder holder, int position) {

        Puzzle puzzle = puzzleList.get(position);

        if (!TextUtils.isEmpty(puzzle.getQuestionUrl())) {
            Picasso.with(context).load(puzzle.getQuestionUrl()).into(holder.questionIV);
        }

        if (!TextUtils.isEmpty(puzzle.getQuestionText())) {
            holder.questionTV.setText(puzzle.getQuestionText());
        }

        if (!TextUtils.isEmpty(puzzle.getAnswerUrl())) {
            Picasso.with(context).load(puzzle.getAnswerUrl()).into(holder.answerIV);
        }

        if (!TextUtils.isEmpty(puzzle.getAnswerText())) {
            holder.answerTV.setText(puzzle.getAnswerText());
        }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView questionIV, answerIV;
        public TextView questionTV, answerTV;


        public MyViewHolder(View itemView) {
            super(itemView);

            questionIV = (ImageView) itemView.findViewById(R.id.questionIV);
            answerIV = (ImageView) itemView.findViewById(R.id.answerIV);
            questionTV = (TextView) itemView.findViewById(R.id.questionTV);
            answerTV = (TextView) itemView.findViewById(R.id.answerTV);
        }
    }
}
